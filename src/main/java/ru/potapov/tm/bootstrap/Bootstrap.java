package ru.potapov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.potapov.tm.api.*;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.command.terminal.TaskReadCommandAbstract;
import ru.potapov.tm.repository.*;
import ru.potapov.tm.service.*;
import java.util.*;



import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class  Bootstrap implements ServiceLocator {
    @NotNull private final IProjectRepository projectRepository= new ProjectRepository();
    @NotNull private final ITaskRepository taskRepository      = new TaskRepository();
    @NotNull private final IUserRepository userRepository      = new UserRepository();

    @NotNull private IProjectService projectService           = new ProjectService(this, projectRepository);
    @NotNull private ITaskService taskService                 = new TaskService(this, taskRepository);
    @NotNull private IUserService userService                 = new UserService(this, userRepository);
    @NotNull private ITerminalService terminalService         = new TerminalService(this);

    @NotNull private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.potapov.tm").getSubTypesOf(AbstractCommand.class);

    public Bootstrap() {}

    public void init() throws Exception{
        try {
            if (getUserService().size() == 0)
                getUserService().loadAllUsers();
        }catch (Exception e){ e.printStackTrace(); }
        if (getUserService().size() == 0)
            getUserService().createPredefinedUsers();


        if (classes.contains(TaskReadCommandAbstract.class))
            classes.remove(TaskReadCommandAbstract.class);

        @NotNull final Class[] CLASSES = new Class[classes.size()];
        classes.toArray(CLASSES);

        terminalService.initCommands(CLASSES);
        try {
            start();
        }catch (Exception e){
            terminalService.printlnArbitraryMassage("Something went wrong...");
            e.printStackTrace();
        }
    }

    private void start() throws Exception{
        terminalService.printlnArbitraryMassage("*** WELCOME TO TASK MANAGER! ***");
        String command = "";
        while (!"exit".equals(command)){
            String[] commands = terminalService.readLine("\nInsert your command in low case or command <help>:").split(" ");
            try {
                execute(commands);
            }catch (Exception e){ e.printStackTrace();}
            command = commands[0];
        }
    }

    private void execute(@Nullable String... command) throws Exception{
        if (Objects.isNull(command) || Objects.isNull(command[0]))
            return;
        String firstParam = command[0];
        if ( Objects.isNull(command) || firstParam.isEmpty())
            return;

        AbstractCommand abstractCommand = terminalService.getMapCommands().get(firstParam);

        if (Objects.isNull(abstractCommand))
            return;

        abstractCommand.execute(command);
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public ITerminalService getTerminalService() {
        return terminalService;
    }
}