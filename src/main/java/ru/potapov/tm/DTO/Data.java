package ru.potapov.tm.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.sun.org.apache.xml.internal.serializer.AttributesImplSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import javax.xml.bind.Element;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/*
 * It's the helper class for all points except couple FasterXML / XML-format. For this couple I use the DataXml.Class
 * */
@Getter
@Setter
@NoArgsConstructor
@XmlRootElement
@JacksonXmlRootElement
public final class Data implements Serializable {
    @JsonProperty("projectMap")
    @JacksonXmlElementWrapper
    @NotNull private Map<String, Project> projectMap    = new HashMap<>();

    @JsonProperty("taskMap")
    @JacksonXmlElementWrapper
    @NotNull private Map<String, Task> taskMap          = new HashMap<>();

    @XmlAnyElement(lax = true)
    @Nullable private List<Element> nodes;
}
