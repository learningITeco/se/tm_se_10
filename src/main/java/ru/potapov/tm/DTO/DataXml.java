package ru.potapov.tm.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import javax.xml.bind.Element;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/*
* It's the helper class only for couple FasterXML / XML-format. For other points I use the Data.Class
* */
@Getter
@Setter
@NoArgsConstructor
@XmlRootElement
@JacksonXmlRootElement
public final class DataXml implements Serializable {
    @JsonProperty("listKeyProject")
    @JacksonXmlElementWrapper
    @NotNull private final List<String> listKeyProject = new ArrayList<>();

    @JsonProperty("listValueProject")
    @JacksonXmlElementWrapper
    @NotNull private final List<Project> listValueProject = new ArrayList<>();

    @JsonProperty("listKeyTask")
    @JacksonXmlElementWrapper
    @NotNull private final List<String> listKeyTask = new ArrayList<>();

    @JsonProperty("listValueTask")
    @JacksonXmlElementWrapper
    @NotNull private final List<Task> listValueTask = new ArrayList<>();

    @XmlAnyElement(lax = true)
    @Nullable
    private List<Element> nodes;
}
