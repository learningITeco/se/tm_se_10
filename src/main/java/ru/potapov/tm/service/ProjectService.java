package ru.potapov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.Project;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {
    public ProjectService(@NotNull ServiceLocator serviceLocator, @NotNull IRepository<Project> repository) {
        super(serviceLocator, repository);
    }

    @Override
    public @NotNull Map<String, Project> getMapRepository() {
        return getRepository().getMapRepository();
    }

    public int checkSize(){
        if (Objects.isNull(getRepository()))
            return 0;
        return getRepository().getCollection().size();
    }

    @Override
    public @Nullable Project findOne(@NotNull String name) {
        if (Objects.isNull(getRepository()))
            return null;
        return getRepository().findOne(name);
    }

    @Override
    public @Nullable Project findOneById(@NotNull String id) {
        if (Objects.isNull(getRepository()))
            return null;
        return getRepository().findOneById(id);
    }

    @Override
    public @Nullable Project findOneById(@NotNull String userId, @NotNull String id) {
        if (Objects.isNull(getRepository()))
            return null;
        return getRepository().findOneById(userId, id);
    }

    @Nullable
    @Override
    public Project findProjectByName(@NotNull final String name){
        if (Objects.isNull(getRepository()))
            return null;
        return getRepository().findOne(name);
    }

    @Nullable
    @Override
    public Project findProjectByName(@NotNull final String userId, @NotNull final String name){
        if (Objects.isNull(getRepository()))
            return null;
        return getRepository().findOne(userId, name);
    }

    @NotNull
    @Override
    public Collection<Project> getCollection(@NotNull final String userId){
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();

        return getRepository().getCollection(userId);
    }

    @NotNull
    @Override
    public Project renameProject(@NotNull final Project project, @Nullable final String name) throws CloneNotSupportedException{
        if (Objects.nonNull(name) && Objects.nonNull(getRepository())){
            Project newProject = (Project) project.clone();
            newProject.setName(name);
            return getRepository().merge(newProject);
        }
        return project;
    }

    @Override
    public void removeAll(@NotNull final String userId){
        if (Objects.isNull(getRepository()))
            return;
        getRepository().removeAll(userId);
    }

    @Override
    public void removeAll(@NotNull final Collection<Project> listProjects){
        if (Objects.isNull(getRepository()))
            return;
        getRepository().removeAll(listProjects);
    }

    @Override
    public void remove(@NotNull final Project project){
        if (Objects.isNull(getRepository()))
            return;
        getRepository().remove(project);
    }

    @Override
    public void put(@NotNull final Project project){
        if (Objects.isNull(getRepository()))
            return;
        getRepository().merge(project);
    }

    public void setMapRepository(@NotNull final Map<String, Project> mapRepository){
        getRepository().setMapRepository(mapRepository);
    };

    @NotNull
    @Override
    public String collectProjectInfo(@NotNull final Project project, @NotNull final String owener, @NotNull final SimpleDateFormat ft){
        String res = "";

        if (Objects.isNull(project.getDateStart()) || Objects.isNull(project.getDateFinish()))
            return res;

        res += "\n";
        res += "Project [" + project.getName() + "]" +  "\n";
        res += "Owner: " + owener + "\n";
        res += "Status: " + project.getStatus() + "\n";
        res += "Description: " + project.getDescription() + "\n";
        res += "ID: " + project.getId() + "\n";
        res += "Date start: " + getServiceLocator().getTerminalService().getFt().format(project.getDateStart() ) + "\n";
        res += "Date finish: " +  getServiceLocator().getTerminalService().getFt().format(project.getDateFinish()) + "\n";

        return res;
    }
}
