package ru.potapov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.*;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @Nullable private MessageDigest md     = null;
    @Nullable private User authorizedUser  = null;
    private boolean isAuthorized           = false;

    public UserService(@NotNull ServiceLocator serviceLocator, @NotNull IRepository<User> repository) {
        super(serviceLocator, repository);
        //digest (MD5):
        try {
            md = MessageDigest.getInstance("MD5");
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
    }

    @Override
    public void setMapRepository(@NotNull Map<String, User> mapRepository) {
        getRepository().setMapRepository(mapRepository);
    }

    @Override
    public void saveAllUsers() throws Exception {
        @NotNull final File file = new File("users.binar");
        @NotNull final ObjectOutputStream inputStream = new ObjectOutputStream(new FileOutputStream(file));
        @NotNull final Map<String, User> mapObj = getMapRepository();
        inputStream.writeObject( mapObj );
    }

    @Override
    public int size() {
        return getCollectionUser().size();
    }

    @Override
    public void loadAllUsers() throws Exception{
        @NotNull final File file = new File("users.binar");
        if (!file.canRead()){
            //getBootstrap().getTerminalService().printlnArbitraryMassage("File cannot be opened");
            return;
        }

        @NotNull final ObjectInputStream inputStream = new ObjectInputStream( new FileInputStream(file) );
        @NotNull final Object objMapUser = inputStream.readObject();
        if (objMapUser instanceof Map){
            @NotNull final Map<String, User> mapUser = (Map<String, User>) objMapUser;
            getServiceLocator().getUserService().setMapRepository(mapUser);
        }    }

    @Override
    public @NotNull Map<String, User> getMapRepository() {
        return getRepository().getMapRepository();
    }

    @NotNull
    @Override
    public User create(@Nullable final String name, @Nullable final String hashPass, @Nullable final RoleType role){
        User user = new User();
        user.setRoleType(role);
        user.setLogin(name);
        user.setHashPass(hashPass);
        user.setId(UUID.randomUUID().toString());

        getRepository().persist(user);

        return user;
    }

    @Nullable
    @Override
    public User getUserByName(@Nullable final String name){
        return getRepository().findOne(name);
    }

    @Nullable
    @Override
    public User getUserById(@Nullable final String id){
        return getRepository().findOneById(id);
    }

    @Override
    public boolean isUserPassCorrect(@Nullable final User user, @Nullable final String hashPass){
        boolean res = false;

        //if ( Arrays.equals(user.getHashPass(), hashPass) )
        if (user.getHashPass().equals(hashPass))
            res = true;

        return res;
    }

    @NotNull
    @Override
    public Collection<User> getCollectionUser(){
        return getRepository().getCollection();
    }

    @NotNull
    @Override
    public User changePass(@Nullable final User user, @Nullable final String newHashPass) throws CloneNotSupportedException{
        User newUser = (User) user.clone();
        newUser.setHashPass(newHashPass);
        return getRepository().merge(newUser);
    }

    @Override
    public void put(@Nullable final User user){
        getRepository().persist(user);
    }

    @NotNull
    @Override
    public String collectUserInfo(@Nullable final User user){
        String res = "";

        res += "\n";
        res += "    User [" + user.getLogin() + "]" +  "\n";
        res += "    ID: " + user.getId() + "\n";
        res += "    Role: " + user.getRoleType() + "\n";

        return res;
    }

    @Override
    public void createPredefinedUsers() {
        String hashPass;
        byte [] hashPassByte;

        hashPassByte = md.digest("1".getBytes());
        hashPass = DatatypeConverter.printHexBinary(hashPassByte);
        create("user",  hashPass, RoleType.User);

        hashPassByte = md.digest("2".getBytes());
        hashPass = DatatypeConverter.printHexBinary(hashPassByte);
        create("admin", hashPass, RoleType.Administrator);
    }
}
