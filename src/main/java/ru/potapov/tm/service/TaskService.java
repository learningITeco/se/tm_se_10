package ru.potapov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.api.ITaskRepository;

import java.text.SimpleDateFormat;
import java.util.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {
    public TaskService(@NotNull ServiceLocator serviceLocator, @NotNull IRepository<Task> repository) {
        super(serviceLocator, repository);
    }

    public int checkSize(){
        if (Objects.isNull(getRepository()))
            return 0;

        return getRepository().getCollection().size();
    }

    @Nullable
    @Override
    public Task findTaskByName(@NotNull final String name){
        Task result = null;
        if (Objects.isNull(getRepository()))
            return result;


        for (@NotNull final Task task : getRepository().getCollection()) {
            if (task.getName().equals(name)) {
                result = task;
                break;
            }
        }
        return result;
    }

    @Override
    public void remove(@NotNull final Task task){
        if (Objects.isNull(getRepository()))
            return;

        getRepository().remove(task);
    }

    @Override
    public void removeAll(@NotNull final String userId){
        if (Objects.isNull(getRepository()))
            return;

        getRepository().removeAll(userId);
    }

    @Override
    public void removeAll(@NotNull final Collection<Task> listTasks){
        if (Objects.isNull(getRepository()))
            return;
        getRepository().removeAll(listTasks);
    }

    @NotNull
    @Override
    public Task renameTask(@NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException{

        if (Objects.nonNull(name) && Objects.nonNull(getRepository())){
            Task newTask = (Task) task.clone();
            newTask.setName(name);
            return getRepository().merge(newTask);
        }
        return task;
    }

    @Override
    public void changeProject(@NotNull final Task task, @Nullable final Project project) throws CloneNotSupportedException{
        if (Objects.nonNull(project) && Objects.nonNull(getRepository())){
            Task newTask = (Task) task.clone();
            newTask.setProjectId(project.getId());
            getRepository().merge(newTask);
        }
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String userId, @NotNull final String projectId){
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();
        return ((ITaskRepository)getRepository()).findAll(userId, projectId);
    }

    @Override
    public @NotNull Collection<Task> findAllByUser(String userId) {
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();
        return ((ITaskRepository) getRepository()).findAllByUser(userId);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String projectId){
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();
        return getRepository().findAll(projectId);
    }

    @Override
    public void put(@NotNull final Task task){
        if (Objects.isNull(getRepository()))
            return;
        getRepository().merge(task);
    }

    @Override
    public @NotNull Map<String, Task> getMapRepository() {
        return getRepository().getMapRepository();
    }

    @Override
    public void setMapRepository(@NotNull final Map<String, Task> mapRepository){
        getRepository().setMapRepository(mapRepository);
    };

    @NotNull
    @Override
    public String collectTaskInfo(@NotNull final Task task){
        String res = "";
        if (Objects.isNull(task.getDateStart()) || Objects.isNull(task.getDateFinish()) || Objects.isNull(getServiceLocator()))
            return res;

        res += "\n";
        res += "    Task [" + task.getName() + "]" +  "\n";
        res += "    Project " + getServiceLocator().getProjectService().findOneById(task.getProjectId())  + "]" +  "\n";
        res += "    Status: " + task.getStatus() + "\n";
        res += "    Description: " + task.getDescription() + "\n";
        res += "    ID: " + task.getId() + "\n";
        res += "    Date start: " +  getServiceLocator().getTerminalService().getFt().format(task.getDateStart() ) + "\n";
        res += "    Date finish: " + getServiceLocator().getTerminalService().getFt().format(task.getDateFinish() ) + "\n";

        return res;
    }
}
