package ru.potapov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ITerminalService;
import ru.potapov.tm.bootstrap.Bootstrap;

import java.text.SimpleDateFormat;
import java.util.*;

import lombok.Getter;
import lombok.Setter;
import ru.potapov.tm.command.AbstractCommand;

@Getter
@Setter
@NoArgsConstructor
public final class TerminalService implements ITerminalService {
    @NotNull final Map<String, AbstractCommand> mapCommand = new LinkedHashMap<>();
    @Nullable private Bootstrap bootstrap;

    @NotNull final SimpleDateFormat            ft            = new SimpleDateFormat("dd-MM-yyyy");
    @NotNull final private Scanner             in            = new Scanner(System.in);

    //Constants
    @NotNull final private String YY                         = "Y";
    @NotNull final private String Yy                         = "y";

    public TerminalService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void initCommands(@NotNull final Class[] CLASSES) {
        for (@NotNull final Class aClass : CLASSES) {
            try {
                regestry((AbstractCommand) aClass.newInstance());
            }catch (Exception e){ e.printStackTrace(); }
        }
    }

    @Override
    public void regestry(@Nullable final AbstractCommand command){
        if ( Objects.isNull(command))
            return;

        @Nullable final String commandName        = command.getName();
        @Nullable final String commandDescription = command.getDescription();

        if (Objects.isNull(commandName) || Objects.isNull(commandDescription)
                || commandName.isEmpty() || commandDescription.isEmpty())
            return;

        command.setServiceLocator(bootstrap);
        mapCommand.put(commandName, command);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getListCommands(){
        return mapCommand.values();
    }

    @NotNull
    @Override
    public Map<String, AbstractCommand> getMapCommands(){
        return mapCommand;
    }

    @NotNull
    @Override
    public Date inputDate(String massage){
        printlnArbitraryMassage(massage);
        String strDate = in.nextLine();
        Date date;
        try {
            date = ft.parse(strDate);
        }catch (Exception e){
            printlnArbitraryMassage("Error formate date! Date set to the end of year.");
            date = new Date();
        }

        return date;
    }

    @NotNull
    @Override
    public String readLine(String msg){
        if (Objects.isNull(bootstrap))
            return "";
        System.out.println(msg);
        String user = (bootstrap.getUserService().isAuthorized()) ? bootstrap.getUserService().getAuthorizedUser().getLogin() + ": " : "";
        System.out.print(user);
        return in.nextLine();
    }

    @Override
    public void printMassageNotAuthorized(){
        printlnArbitraryMassage("You are not authorized, plz login (type command <user-login>)");
    }

    @Override
    public void printMassageCompleted(){
        printlnArbitraryMassage("Completed");
    }

    @Override
    public void printMassageOk(){
        printlnArbitraryMassage("Ok");
    }

    @Override
    public void printlnArbitraryMassage(String msg){
        printArbitraryMassage(msg + "\n");
    }

    @Override
    public void printArbitraryMassage(String msg){
        System.out.print(msg);
    }
}
