package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IEntity;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ServiceLocator;


@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractService<T extends IEntity> {
    @Nullable private IRepository<T> repository;
    @Nullable private ServiceLocator serviceLocator;

    public AbstractService(@NotNull ServiceLocator serviceLocator, @NotNull IRepository<T> repository) {
        this.serviceLocator = serviceLocator;
        this.repository     = repository;
    }
}
