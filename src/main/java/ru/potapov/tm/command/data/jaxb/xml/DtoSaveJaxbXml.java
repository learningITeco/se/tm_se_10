package ru.potapov.tm.command.data.jaxb.xml;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.DTO.Data;
import ru.potapov.tm.command.AbstractCommand;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

import java.io.FileWriter;

@Setter
@Getter
@NoArgsConstructor
public class DtoSaveJaxbXml extends AbstractCommand {
    public DtoSaveJaxbXml(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        return "save-jaxb-xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saves in xml-format by JAXB";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;

        @NotNull final Data data = new Data() ;
        data.setProjectMap( getServiceLocator().getProjectService().getMapRepository() );
        data.setTaskMap( getServiceLocator().getTaskService().getMapRepository() );

        @NotNull final File file = new File("data-jaxb.xml");
        @NotNull final JAXBContext context      = JAXBContext.newInstance(Data.class) ;
        @NotNull final Marshaller marshaller    = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(data, new FileWriter(file));
        getServiceLocator().getTerminalService().printMassageCompleted();
    }


}
