package ru.potapov.tm.command.data.fasterxml.xml;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.DTO.DataXml;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@NoArgsConstructor
public class DtoLoadFasterXml extends AbstractCommand {
    public DtoLoadFasterXml(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        return "load-faster-xml";

    }

    @Override
    public @NotNull String getDescription() {
        return "Loads from xml-format by FasterXML";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        @NotNull final File file = new File("data-faster.xml");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        @NotNull final DataXml data = mapper.readValue(file, DataXml.class);

        Map<String, Project> mapProject = new HashMap<>();
        Map<String, Task> mapTask       = new HashMap<>();
        if ( (data.getListKeyProject().size() > 0) && (data.getListValueProject().size() > 0) ){
            for (int i = 0; i < data.getListKeyProject().size(); i++) {
                mapProject.put(data.getListKeyProject().get(i), data.getListValueProject().get(i));
            }
            getServiceLocator().getProjectService().setMapRepository( mapProject );
        }

        if ( (data.getListKeyTask().size() > 0) && (data.getListValueTask().size() > 0) ){
            for (int i = 0; i < data.getListKeyTask().size(); i++) {
                mapTask.put(data.getListKeyTask().get(i), data.getListValueTask().get(i));
            }
            getServiceLocator().getTaskService().setMapRepository( mapTask );
        }

        getServiceLocator().getTerminalService().printMassageCompleted();
    }

    private String inputStreamToString( @NotNull final InputStream is) throws IOException {
        @NotNull final StringBuilder sb = new StringBuilder();
        @NotNull String line;
        @NotNull final BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }
}
