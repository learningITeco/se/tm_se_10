package ru.potapov.tm.command.data.fasterxml.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.DTO.Data;

import java.io.File;

@NoArgsConstructor
public class DtoLoadFasterJson extends AbstractCommand {
    public DtoLoadFasterJson(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        return "load-faster-json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Loads from json-format by FasterXML";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;

        @NotNull final File file = new File("data-faster.json");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final Data data = mapper.readValue(file, Data.class);

        if (data.getProjectMap().size() > 0)
            getServiceLocator().getProjectService().setMapRepository( data.getProjectMap() );
        if (data.getTaskMap().size() > 0)
            getServiceLocator().getTaskService().setMapRepository( data.getTaskMap() );

        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
