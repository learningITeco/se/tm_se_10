package ru.potapov.tm.command.data.binar;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.DTO.Data;
import ru.potapov.tm.command.AbstractCommand;

import java.io.*;

@NoArgsConstructor
public final class DtoSaveBinCommand extends AbstractCommand {
    public DtoSaveBinCommand(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        return "save-binar";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saves in binary file";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;

        @NotNull final File file = new File("data.binar");
        @NotNull final ObjectOutputStream inputStream = new ObjectOutputStream(new FileOutputStream(file));

        @NotNull final Data data = new Data();
        data.setProjectMap( getServiceLocator().getProjectService().getMapRepository() );
        data.setTaskMap( getServiceLocator().getTaskService().getMapRepository() );

        inputStream.writeObject( data );
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
