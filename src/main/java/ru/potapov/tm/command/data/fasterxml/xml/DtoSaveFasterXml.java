package ru.potapov.tm.command.data.fasterxml.xml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.DTO.DataXml;

import java.io.File;

@NoArgsConstructor
public class DtoSaveFasterXml extends AbstractCommand {
    public DtoSaveFasterXml(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        return "save-faster-xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saves in xml-format by FasterXML";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;

        DataXml dataXml = new DataXml();
        dataXml.getListKeyProject().addAll(getServiceLocator().getProjectService().getMapRepository().keySet());
        dataXml.getListValueProject().addAll(getServiceLocator().getProjectService().getMapRepository().values());
        dataXml.getListKeyTask().addAll(getServiceLocator().getTaskService().getMapRepository().keySet());
        dataXml.getListValueTask().addAll(getServiceLocator().getTaskService().getMapRepository().values());

        @NotNull final File file = new File("data-faster.xml");
        @NotNull final XmlMapper  mapper = new XmlMapper();
        mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, dataXml);
        //mapper.writeValue(file, data);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
