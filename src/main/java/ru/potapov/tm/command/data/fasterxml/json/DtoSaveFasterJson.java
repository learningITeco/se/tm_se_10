package ru.potapov.tm.command.data.fasterxml.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.DTO.Data;

import java.io.File;
import java.io.FileOutputStream;

@NoArgsConstructor
public class DtoSaveFasterJson extends AbstractCommand {
    public DtoSaveFasterJson(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        return "save-faster-json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saves in json-format by FasterXML";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;

        @NotNull final Data data = new Data() ;
        data.setProjectMap( getServiceLocator().getProjectService().getMapRepository() );
        data.setTaskMap( getServiceLocator().getTaskService().getMapRepository() );

        @NotNull final File file = new File("data-faster.json");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        String s = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(data);
        FileOutputStream outputStream = new FileOutputStream(file);
        outputStream.write(s.getBytes());
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
