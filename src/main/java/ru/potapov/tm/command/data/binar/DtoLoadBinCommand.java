package ru.potapov.tm.command.data.binar;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.DTO.Data;
import ru.potapov.tm.command.AbstractCommand;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

@NoArgsConstructor
public class DtoLoadBinCommand extends AbstractCommand {
    public DtoLoadBinCommand(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        setNeedAuthorize(true);
        return "load-binar";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saves in binary file";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;

        @NotNull final File file = new File("data.binar");
        if (!file.canRead()){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("File cannot be opened");
            return;
        }

        @NotNull final ObjectInputStream inputStream = new ObjectInputStream( new FileInputStream(file) );
        @NotNull final Object dataObj = inputStream.readObject();
        if (dataObj instanceof Data){
            @NotNull final Data data = (Data) dataObj;
            getServiceLocator().getProjectService().setMapRepository(data.getProjectMap());
            getServiceLocator().getTaskService().setMapRepository(data.getTaskMap());
        }
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
