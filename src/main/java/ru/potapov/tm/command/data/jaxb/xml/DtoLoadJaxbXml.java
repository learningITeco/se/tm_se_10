package ru.potapov.tm.command.data.jaxb.xml;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.DTO.Data;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class DtoLoadJaxbXml extends AbstractCommand {
    public DtoLoadJaxbXml(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        return "load-jaxb-xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Loads in xml-format by JAXB";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        @NotNull final File file = new File("data-jaxb.xml");

        @NotNull final JAXBContext context = JAXBContext.newInstance(Data.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Data data =(Data) unmarshaller.unmarshal(file);

        if (data.getProjectMap().size() > 0)
            getServiceLocator().getProjectService().setMapRepository( data.getProjectMap() );
        if (data.getTaskMap().size() > 0)
            getServiceLocator().getTaskService().setMapRepository( data.getTaskMap() );

        getServiceLocator().getTerminalService().printMassageCompleted();
    }


}
