package ru.potapov.tm.api;

import org.jetbrains.annotations.Nullable;

public interface IEntity {
    @Nullable String getId();
    @Nullable String getName();
    @Nullable String getUserId();
}
