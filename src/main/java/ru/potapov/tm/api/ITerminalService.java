package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.command.AbstractCommand;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;

public interface ITerminalService {
    void initCommands(final Class[] CLASSES);
    void regestry(@NotNull final AbstractCommand command);
    @NotNull Collection<AbstractCommand> getListCommands();
    @NotNull Map<String, AbstractCommand> getMapCommands();
    @Nullable Date inputDate(@NotNull final String massage);
    @NotNull Scanner getIn() ;
    @NotNull SimpleDateFormat getFt() ;
    @NotNull String readLine(@NotNull final String msg);
    void printMassageNotAuthorized();
    void printMassageCompleted();
    void printMassageOk();
    void printlnArbitraryMassage(@NotNull final String msg);
    void printArbitraryMassage(@NotNull final String msg);
}
