package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.command.terminal.SortBy;

public interface IEntityRead {
    void setSort(@NotNull final SortBy sortBy, @Nullable final String PartOfNameOrDiscript);
}
