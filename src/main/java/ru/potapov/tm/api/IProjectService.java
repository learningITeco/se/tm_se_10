package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Map;

public interface IProjectService {
    int checkSize();
    @Nullable Project findOne(@NotNull final String name);
    @Nullable Project findProjectByName(@NotNull final String name);
    @Nullable Project findProjectByName(@NotNull final String userId, @NotNull final String name);
    @Nullable Project findOneById(@NotNull final String id);
    @Nullable Project findOneById(@NotNull final String userId, @NotNull final String id);
    @NotNull Collection<Project> getCollection(@NotNull final String userId);
    @NotNull Project renameProject(@NotNull final Project project, @NotNull final String name) throws CloneNotSupportedException;
    void removeAll(@NotNull final String userId);
    void removeAll(Collection<Project> listProjects);
    void remove(@NotNull final Project project);
    void put(@NotNull final Project project);
    @NotNull String collectProjectInfo(@NotNull final Project project, @NotNull final String owener, @NotNull final SimpleDateFormat ft);
    void setMapRepository(@NotNull final Map<String, Project> mapRepository);
    @NotNull Map<String, Project> getMapRepository();
}
